package com.microservice.commandesservice.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Commande {

    @Id
    private int id;
    private int productId;
    private int commandPrice;
    private int quantity;
    private String commandDate;

    public Commande() {}

    public Commande(int id, int productId, int commandPrice, int quantity, String commandDate) {
        this.id = id;
        this.productId = productId;
        this.commandPrice = commandPrice;
        this.quantity = quantity;
        this.commandDate = commandDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getCommandPrice() {
        return commandPrice;
    }

    public void setCommandPrice(int commandPrice) {
        this.commandPrice = commandPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCommandDate() {
        return commandDate;
    }

    public void setCommandDate(String commandDate) {
        this.commandDate = commandDate;
    }

}
