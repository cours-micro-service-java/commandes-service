package com.microservice.commandesservice.web.controller;

import com.microservice.commandesservice.dao.CommandDao;
import com.microservice.commandesservice.model.Commande;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@Api(description = "Api pour les opérations CRUD")
public class CommandController {

    @Autowired
    private CommandDao commandDao;

    @GetMapping(value = "/commandes")
    public List<Commande> listArticles() {
        return commandDao.findAll();
    }

    @GetMapping(value = "/commandes/{id}")
    public Commande getCommandById(@PathVariable int id) {
        return commandDao.findById(id);
    }

    @PostMapping(value = "/commandes")
    public ResponseEntity<Void> addCommand(@RequestBody Commande commande) {

        Commande commandAdded = commandDao.save(commande);

        if (commandAdded == null) {
            return ResponseEntity.noContent().build();
        } else {
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(commandAdded.getId()).toUri();
            return ResponseEntity.created(location).build();
        }
    }

}
